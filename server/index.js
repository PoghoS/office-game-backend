// Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

var port = process.env.PORT || 3000;

server.listen(port, () => {
    console.log('Server listening at port %d', port);
});

let codeSocketMap = {};
let activeGamesSockets = {};
let firstPlayerName;

io.on('connection', (socket) => {
    let randomString = generateRandomCode();
    socket.emit("code", randomString);
    codeSocketMap[randomString] = socket;

    socket.on('partnerCoords', function (data) {
        let playerData = JSON.parse(data);

        // console.log("playerData: ", playerData);
        //
        // let roomSockets = activeGamesSockets[playerData.room];
        // if (!roomSockets) return;
        //
        // let targetSocket =
        //     (roomSockets.socket1 === socket) ? roomSockets.socket2 : roomSockets.socket1;
        //
        // targetSocket.emit('RECEIVE_PARTNER_COORDS', playerData);

        getTargetSocket('receivePartnerCoords', playerData.room, playerData, socket);
    });

    socket.on("hit", function (data) {
        let damageData = JSON.parse(data);
        getTargetSocket("hitReceived", damageData.room, damageData, socket);
    });

    console.log("connection: ", "socket id", socket.id);

    socket.on('friendlyMatch', function (data) {
        if (data && data.code) {
            let partnerSocket = codeSocketMap[data.code];
            if (!partnerSocket) {
                return;
            }

            socket.join(data.code);
            partnerSocket.join(data.code);

            socket.broadcast.to(data.code).emit("init", [partnerSocket.id, socket.id]);
            // io.emit("init", [partnerSocket.id, socket.id]);

            activeGamesSockets[data.code] = {
                socket1: socket,
                socket2: partnerSocket
            };
            delete codeSocketMap[data.code];
        }
    });

    socket.on('match', function (jsonData) {
        let data = JSON.parse(jsonData);
        if (!data.code && data.playerName) {

            if (!firstPlayerName) {
                firstPlayerName = data.playerName;
            }

            let keys = Object.keys(codeSocketMap);

            if (keys.length > 1) {

                let firstSocket = codeSocketMap[keys[0]];
                let secondSocket = codeSocketMap[keys[1]];

                let room = keys[0];
                firstSocket.join(room);
                secondSocket.join(room);

                // io.emit("init", [first.id, second.id]);
                // socket.broadcast.to(room).emit("init", [firstSocket.id, secondSocket.id]);
                io.to(room).emit("init", {
                    room,
                    first: {
                        code: keys[0],
                        name: firstPlayerName
                    },
                    second: {
                        code: keys[1],
                        name: data.playerName
                    }
                });
                firstPlayerName = null;

                activeGamesSockets[room] = {
                    socket1: firstSocket,
                    socket2: secondSocket
                };

                delete codeSocketMap[keys[0]];
                delete codeSocketMap[keys[1]];
            }
        }
    });

    socket.on('disconnect', () => {

    });
});

function getTargetSocket(eventName, room, sendData, socket) {
    let roomSockets = activeGamesSockets[room];
    if (!roomSockets) return;
    let targetSocket =
        (roomSockets.socket1 === socket) ? roomSockets.socket2 : roomSockets.socket1;

    targetSocket.emit(eventName, sendData);
}

function generateRandomCode() {
    return Math.random().toString(36).substring(7);
}