const io = require('socket.io-client');
const roomName = process.argv[2];
const socket = io('http://localhost:3000', {roomName});
console.log(roomName);

socket.emit("test", roomName);
socket.on('room_join', function (data) {
    console.log('room_join', data);
});

let code;

socket.on('code', function (data) {
    code = data;
    console.log("code", data);
    console.log("roomName", roomName);
    if (roomName) {
        // friendlyPlay(socket);
        play(socket);
    }

});

socket.on('init', function (data) {
    console.log('init', data);
});

socket.on('join', function (data) {
    console.log('socket join', data);
});

socket.on("error_detected", function (data) {

});

function friendlyPlay(socket) {
    socket.emit("friendlyMatch", {code});
}

function play(socket) {
    socket.emit("match");
}

